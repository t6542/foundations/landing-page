# Landing Page

This is one part of the practice projects given by [Odin Project](https://www.theodinproject.com/).
This particular project is the very first one from the [Foundations](https://www.theodinproject.com/paths/foundations/courses/foundations) course.
A simple project to practice css and SMACSS css methodology.

The site itself is based on a given [layout](https://cdn.statically.io/gh/TheOdinProject/curriculum/main/foundations/html_css/project/odin-project.png) and [design](https://cdn.statically.io/gh/TheOdinProject/curriculum/main/foundations/html_css/project/colors_and_stuff.png)

## Projects Purpose

This is again a super simple landing page, but its purpose is to practice not only the programing, but project management and css methodology.

## Installation

There is nothing to install as it is just some html files.

[Live preview](https://sad-agnesi-569191.netlify.app/)
